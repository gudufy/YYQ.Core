﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YYQ.Core
{
    public class DataFilter
    {
        /// <summary>
        /// 类型
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// 字段
        /// </summary>
        public string field { get; set; }
        /// <summary>
        /// comparison
        /// </summary>
        public string op { get; set; }
    }

    public class DataFilterGroup
    {
        /// <summary>
        /// or /and
        /// </summary>
        public string Operation { get; set; }
        public List<DataFilter> Filters { get; set; }
    }
}
