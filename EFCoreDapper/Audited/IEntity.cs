﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace YYQ.Core.Audited
{
    /// <summary>
    /// IEntity
    /// </summary>
    public interface IEntity
    {

    }

    public interface IEntity<TPrimaryKey>: IEntity
    {
        [Key]
        TPrimaryKey Id { get; set; }
    }
}
