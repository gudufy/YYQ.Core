﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YYQ.Core.Audited
{
    public interface IMultiLingualEntity<TTranslation>
        where TTranslation : class, IEntityTranslation
    {
        List<TTranslation> Translations { get; set; }
    }
}
