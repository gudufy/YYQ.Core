﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyProject.Core
{
    public static class ColumnSetting
    {
        public const int Short = 50;
        public const int Longer = 200;
        public const int Long = 100;
        public const int Remark = 300;
        public const int Url = 300;
        public const int Json = 500;
        public const int VeryShort = 20;
        public const int SmallShort = 10;
        public const int Phone = 15;
    }
}
