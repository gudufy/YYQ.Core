﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace YYQ.Core.Common
{
    /// <summary>
    /// Image resize mode
    /// </summary>
    public enum ImageResizeMode
    {
        /// <summary>
        /// Resize to the specified size, allow aspect ratio change
        /// </summary>
        Fixed,
        /// <summary>
        /// Resize to the specified width, height is calculated by the aspect ratio
        /// </summary>
        ByWidth,
        /// <summary>
        /// Resize to the specified height, width is calculated by the aspect ratio
        /// </summary>
        ByHeight,
        /// <summary>
        /// Resize to the specified size, keep aspect ratio and cut the overflow part
        /// </summary>
        Cut,
        /// <summary>
        /// Resize to the specified size, keep aspect ratio and padding the insufficient part
        /// </summary>
        Padding
    }

    public class ImageHelper
    {
        /// <summary>
        /// 压缩图片
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="dHeight"></param>
        /// <param name="dWidth"></param>
        /// <param name="flag">压缩质量1-100  </param>
        /// <param name="outstream"></param>
        /// <returns></returns>
        public static bool GetPicThumbnail(Stream stream, ImageResizeMode mode, int height, int width, int flag, Stream outstream)
        {
            //可以直接从流里边得到图片,这样就可以不先存储一份了
            System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

            var src = new Rectangle(0, 0, image.Width, image.Height);
            var dst = new Rectangle(0, 0, width, height);
            // Calculate destination rectangle by resize mode
            if (mode == ImageResizeMode.Fixed)
            {
            }
            else if (mode == ImageResizeMode.ByWidth)
            {
                height = (int)((decimal)src.Height / src.Width * dst.Width);
                dst.Height = height;
            }
            else if (mode == ImageResizeMode.ByHeight)
            {
                width = (int)((decimal)src.Width / src.Height * dst.Height);
                dst.Width = width;
            }
            else if (mode == ImageResizeMode.Cut)
            {
                if ((decimal)src.Width / src.Height > (decimal)dst.Width / dst.Height)
                {
                    src.Width = (int)((decimal)dst.Width / dst.Height * src.Height);
                    src.X = (image.Width - src.Width) / 2; // Cut left and right
                }
                else
                {
                    src.Height = (int)((decimal)dst.Height / dst.Width * src.Width);
                    src.Y = (image.Height - src.Height) / 2; // Cut top and bottom
                }
            }
            else if (mode == ImageResizeMode.Padding)
            {
                if ((decimal)src.Width / src.Height > (decimal)dst.Width / dst.Height)
                {
                    dst.Height = (int)((decimal)src.Height / src.Width * dst.Width);
                    dst.Y = (height - dst.Height) / 2; // Padding left and right
                }
                else
                {
                    dst.Width = (int)((decimal)src.Width / src.Height * dst.Height);
                    dst.X = (width - dst.Width) / 2; // Padding top and bottom
                }
            }

            ImageFormat tFormat = image.RawFormat;


            Bitmap ob = new Bitmap(width, height);
            using (var g = Graphics.FromImage(ob))
            {
                g.Clear(Color.WhiteSmoke);
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(image, dst, src, GraphicsUnit.Pixel);

                //以下代码为保存图片时，设置压缩质量  
                EncoderParameters ep = new EncoderParameters();
                long[] qy = new long[1];
                qy[0] = flag;//设置压缩的比例1-100  
                EncoderParameter eParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qy);
                ep.Param[0] = eParam;
                try
                {
                    ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();
                    ImageCodecInfo jpegICIinfo = null;
                    for (int x = 0; x < arrayICI.Length; x++)
                    {
                        if (arrayICI[x].FormatDescription.Equals("JPEG"))
                        {
                            jpegICIinfo = arrayICI[x];
                            break;
                        }
                    }
                    if (jpegICIinfo != null)
                    {
                        //可以存储在流里边;
                        ob.Save(outstream, jpegICIinfo, ep);

                    }
                    else
                    {
                        ob.Save(outstream, tFormat);
                    }
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    image.Dispose();
                    ob.Dispose();
                }
            }
        }
    }
}
