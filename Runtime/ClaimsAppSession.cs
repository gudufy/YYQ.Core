﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using YYQ.Core;

namespace YYQ.Core.Runtime
{
    public class ClaimsAppSession: IAppSession
    {
        public int? UserId
        {
            get
            {
                var userIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst(c => c.Type == AppClaimTypes.UserId);
                if (string.IsNullOrEmpty(userIdClaim?.Value))
                {
                    return null;
                }

                if (!int.TryParse(userIdClaim.Value, out int userId))
                {
                    return null;
                }

                return userId;
            }
        }

        public string UserName
        {
            get
            {
                var userIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst(c => c.Type == AppClaimTypes.UserName);
                if (string.IsNullOrEmpty(userIdClaim?.Value))
                {
                    return null;
                }

                return userIdClaim.Value.ToString();
            }
        }

        public int? TenantId
        {
            get
            {
                var tenantIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst(c => c.Type == AppClaimTypes.TenantId);
                if (!string.IsNullOrEmpty(tenantIdClaim?.Value))
                {
                    return Convert.ToInt32(tenantIdClaim.Value);
                }

                if (UserId == null)
                {
                    //Resolve tenant id from request only if user has not logged in!
                    return null;
                }

                return null;
            }
        }

        public int? TeamId
        {
            get
            {
                var tenantIdClaim = _httpContextAccessor.HttpContext.User?.FindFirst(c => c.Type == AppClaimTypes.TeamId);
                if (!string.IsNullOrEmpty(tenantIdClaim?.Value))
                {
                    return Convert.ToInt32(tenantIdClaim.Value);
                }

                if (UserId == null)
                {
                    //Resolve tenant id from request only if user has not logged in!
                    return null;
                }

                return null;
            }
        }

        public List<string> Roles {
            get
            {
                return _httpContextAccessor.HttpContext.User?.Claims.Where(c => c.Type == AppClaimTypes.Role).Select(x=>x.Value).ToList();
            }
        }

        protected IHttpContextAccessor _httpContextAccessor { get; }

        public string Language
        {
            get
            {
                var lang = _httpContextAccessor.HttpContext.Request.Headers["language"];
                return string.IsNullOrEmpty(lang) ? "zh-CN" : lang.ToString();
            }
        }

        public string ClientIp
        {
            get
            {
                return _httpContextAccessor.HttpContext.Connection.LocalIpAddress.ToString();
            }
        }

        public ClaimsAppSession(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
    }
}
