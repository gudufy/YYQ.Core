﻿using System;
using System.Security.Claims;
using System.Text;

namespace YYQ.Core.Runtime
{
    public static class AppClaimTypes
    {
        public static string UserName { get; set; } = ClaimTypes.Name;

        public static string UserId { get; set; } = ClaimTypes.NameIdentifier;

        public static string Role { get; set; } = ClaimTypes.Role;

        public static string TenantId { get; set; } = "http://www.aspnetboilerplate.com/identity/claims/tenantId";

        public static string TeamId { get; set; } = "http://www.aspnetboilerplate.com/identity/claims/teamId";
    }
}
