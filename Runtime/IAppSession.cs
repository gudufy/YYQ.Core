﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YYQ.Core.Runtime
{
    public interface IAppSession
    {
        /// <summary>
        /// Gets current UserId or null.
        /// It can be null if no user logged in.
        /// </summary>
        int? UserId { get; }

        /// <summary>
        /// Gets current TenantId or null.
        /// This TenantId should be the TenantId of the <see cref="UserId"/>.
        /// It can be null if given <see cref="UserId"/> is a host user or no user logged in.
        /// </summary>
        int? TenantId { get; }

        int? TeamId { get; }

        string UserName { get; }

        List<string> Roles { get; }

        string Language { get; }

        string ClientIp { get; }
    }
}
