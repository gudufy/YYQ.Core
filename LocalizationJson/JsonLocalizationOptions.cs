﻿using Microsoft.Extensions.Localization;

namespace YYQ.Core.Localization.Json
{
    public class JsonLocalizationOptions : LocalizationOptions
    {
        public ResourcesType ResourcesType { get; set; } = ResourcesType.TypeBased;
    }
}