﻿namespace YYQ.Core.Localization.Json
{
    public enum ResourcesType
    {
        CultureBased,
        TypeBased
    }
}