﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YYQ.Core.Extensions
{
    public static class DateTimeExtensions
    {
         /// <summary>
         /// 日期转换为时间戳（时间戳单位秒）
         /// </summary>
         /// <param name="TimeStamp"></param>
         /// <returns></returns>
         public static long ToTimeStamp(this DateTime time)
         {
             DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
             return (long) (time.AddHours(-8) - Jan1st1970).TotalMilliseconds;
         }
 
         /// <summary>
         /// 时间戳转换为日期（时间戳单位秒）
         /// </summary>
         /// <param name="TimeStamp"></param>
         /// <returns></returns>
         public static DateTime ToDateTime(this long timeStamp)
         {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            long tt = dt.Ticks + timeStamp * 10000;
            return new DateTime(tt);
        }

        /// <summary>
        /// 10位时间戳转为C#格式时间
        /// </summary>
        /// <param name=”timeStamp”></param>
        /// <returns></returns>
        public static DateTime ToDateTimeFrom10(this long timeStamp)
        {
            Int64 begtime = timeStamp * 10000000;
            DateTime dt_1970 = new DateTime(1970, 1, 1, 8, 0, 0);
            long tricks_1970 = dt_1970.Ticks;//1970年1月1日刻度
            long time_tricks = tricks_1970 + begtime;//日志日期刻度
            DateTime dt = new DateTime(time_tricks);//转化为DateTime
            return dt;
        }
    }
}
